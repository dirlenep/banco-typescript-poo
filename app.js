"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var banco_1 = require("./banco");
var cliente_1 = require("./cliente");
var conta_1 = require("./conta");
var app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
var porta = 3001;
app.listen(porta, function () {
    console.log("Servidor rodando na porta " + porta);
});
app.get("/", function (req, resp) {
    resp.json({ msn: "Servidor rodando" });
});
var conta;
var cliente;
var banco;
app.post("/banco", function (req, resp) {
    var bancoUI = {
        nome: req.body.nmNomeBanco,
        cnpj: req.body.nmCnpj,
        agencia: req.body.nmAgencia
    };
    banco = new banco_1.Banco(bancoUI.nome, bancoUI.cnpj, bancoUI.agencia, []);
    resp.send(banco);
});
app.get("/banco", function (req, resp) {
    resp.json({ msn: banco });
});
app.post("/conta", function (req, resp) {
    var contaUI = {
        numConta: req.body.nmConta,
        saldo: req.body.nmSaldo,
        limite: req.body.nmLimite,
        tipo: req.body.nmTipo,
        senha: req.body.nmSenha
    };
    conta = new conta_1.Conta(contaUI.numConta, contaUI.saldo, contaUI.limite, contaUI.tipo, contaUI.senha);
    resp.send(conta);
});
app.get("/conta", function (req, resp) {
    resp.json({ msn: conta });
});
app.post("/cliente", function (req, resp) {
    var clienteUI = {
        nome: req.body.nmNomeCliente,
        cpf: req.body.nmCpf,
        rg: req.body.nmRg,
        endereco: req.body.nmEndereco
    };
    cliente = new cliente_1.Cliente(clienteUI.nome, clienteUI.cpf, clienteUI.rg, clienteUI.endereco, conta);
    banco.adicionaCliente(cliente);
    resp.send(cliente);
});
app.get("/cliente", function (req, resp) {
    resp.json({ msn: cliente });
});
app.post("/depositar", function (req, resp) {
    var clienteSelecionado = banco.getCliente(req.body.nmContaCliente);
    var deposito = req.body.nmDeposito;
    clienteSelecionado.conta.depositar(deposito);
    resp.json({ deposito: deposito });
});
app.post("/sacar", function (req, resp) {
    var clienteSelecionado = banco.getCliente(req.body.nmContaCliente);
    var saque = req.body.nmSaque;
    clienteSelecionado.conta.sacar(saque);
    resp.json({ saque: saque });
});
app.post("/extrato", function (req, resp) {
    var clienteSelecionado = banco.getCliente(req.body.nmContaCliente);
    var saldo = (clienteSelecionado.conta.saldo).toFixed(2);
    var limite = (clienteSelecionado.conta.limite).toFixed(2);
    resp.json({ msn: "Saldo: R$" + saldo + "  |  Limite: R$" + limite });
});
app.get("/extrato", function (req, resp) {
    var clienteSelecionado = banco.getCliente(req.query.nmContaCliente);
    var saldo = (clienteSelecionado.conta.saldo).toFixed(2);
    var limite = (clienteSelecionado.conta.limite).toFixed(2);
    resp.json({ msn: "Saldo: R$" + saldo + "  |  Limite: R$" + limite });
});
