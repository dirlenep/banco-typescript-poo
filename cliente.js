"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cliente = void 0;
var Cliente = (function () {
    function Cliente(nome, cpf, rg, endereco, conta) {
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.endereco = endereco;
        this.conta = conta;
    }
    Object.defineProperty(Cliente.prototype, "nome", {
        get: function () {
            return this._nome;
        },
        set: function (value) {
            this._nome = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "cpf", {
        get: function () {
            return this._cpf;
        },
        set: function (value) {
            if (this.validarCpf(value)) {
                this._cpf = value;
            }
            else {
                throw new Error("Erro ao salvar CPF");
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "rg", {
        get: function () {
            return this._rg;
        },
        set: function (value) {
            this._rg = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "endereco", {
        get: function () {
            return this._endereco;
        },
        set: function (value) {
            this._endereco = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "conta", {
        get: function () {
            return this._conta;
        },
        set: function (value) {
            this._conta = value;
        },
        enumerable: false,
        configurable: true
    });
    Cliente.prototype.validarCpf = function (value) {
        if (value == "") {
            return false;
        }
        return true;
    };
    return Cliente;
}());
exports.Cliente = Cliente;
