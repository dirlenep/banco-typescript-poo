"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Conta = void 0;
var Conta = (function () {
    function Conta(numConta, saldo, limite, tipo, senha) {
        this.numConta = numConta;
        this._saldo = saldo;
        this._limite = limite;
        this.tipo = tipo;
        this.senha = senha;
    }
    Object.defineProperty(Conta.prototype, "numConta", {
        get: function () {
            return this._numConta;
        },
        set: function (value) {
            this._numConta = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "saldo", {
        get: function () {
            return this._saldo;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "limite", {
        get: function () {
            return this._limite;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "tipo", {
        get: function () {
            return this._tipo;
        },
        set: function (value) {
            this._tipo = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "senha", {
        get: function () {
            return this._senha;
        },
        set: function (value) {
            this._senha = value;
        },
        enumerable: false,
        configurable: true
    });
    Conta.prototype.depositar = function (deposito) {
        if (deposito > 0) {
            this._saldo = this._saldo + deposito;
            return this._saldo;
        }
    };
    Conta.prototype.sacar = function (saque) {
        if (saque <= this._saldo) {
            this._saldo = this._saldo - saque;
            return this._saldo;
        }
        else if (this._limite > 0 && saque <= this._limite) {
            this._saldo = this._saldo - saque;
            this._limite = this.limite - Math.abs(this._saldo);
            return this._saldo;
        }
        else {
            throw new Error("Sem limite para saque!");
        }
    };
    return Conta;
}());
exports.Conta = Conta;
