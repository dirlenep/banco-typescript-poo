import { Conta } from "./conta";

export class Cliente {
    private _nome: string;
    private _cpf: string;
    private _rg: string;
    private _endereco: string;
    private _conta: Conta;

    constructor(nome: string, cpf: string, rg: string, endereco: string, conta: Conta) {
        this.nome = nome;
        this.cpf = cpf; 
        this.rg = rg;
        this.endereco = endereco; 
        this.conta = conta;
    }

    public get nome(): string {
        return this._nome;
    }
    public set nome(value: string) {
        this._nome = value;
    }

    public get cpf(): string {
        return this._cpf;
    }
    public set cpf(value: string) {
        if (this.validarCpf(value)) {
            this._cpf = value; 
        } else {
            throw new Error("Erro ao salvar CPF");
        }
    }

    public get rg(): string {
        return this._rg;
    }
    public set rg(value: string) {
        this._rg = value;
    }

    public get endereco(): string {
        return this._endereco;
    }
    public set endereco(value: string) {
        this._endereco = value;
    }

    public get conta(): Conta {
        return this._conta;
    }
    public set conta(value: Conta) {
        this._conta = value;
    }

    private validarCpf(value: string) : boolean{
        if (value == "") {
           return false 
        }
        return true
    }
}