import { Cliente } from "./cliente";

export class Banco {
    private _nome: string;
    private _cnpj: string;
    private _agencia: number;
    private _clientes: Cliente[];

    constructor(nome: string, cnpj: string, agencia: number, clientes: Cliente[]) {
        this.nome = nome;
        this.cnpj = cnpj; 
        this.agencia = agencia;
        this.clientes = [];
        //this.clientes = clientes;
    }

    public get nome(): string {
        return this._nome;
    }
    public set nome(value: string) {
        this._nome = value;
    }

    public get cnpj(): string {
        return this._cnpj;
    }
    public set cnpj(value: string) {
        this._cnpj = value;
    }

    public get agencia(): number {
        return this._agencia;
    }
    public set agencia(value: number) {
        this._agencia = value;
    }

    public get clientes(): Cliente[] {
        return this._clientes;
    }
    public set clientes(value: Cliente[]) {
        this._clientes = value;
    }

    public adicionaCliente(value: Cliente){
        this.clientes.push(value);
    }

    public getCliente(value: string){
        for (let i = 0; i < this.clientes.length; i++) {
            if (this.clientes[i].cpf == value) {
                return this.clientes[i];
            }
        }
    }
}