export class Conta {
    private _numConta: number;
    private _saldo: number;
    private _limite: number;
    private _tipo: number;
    private _senha: number;

    constructor(numConta: number, saldo: number, limite: number, tipo: number, senha: number) {
        this.numConta = numConta;
        this._saldo = saldo; 
        this._limite = limite;
        this.tipo = tipo; 
        this.senha = senha;
    }

    public get numConta(): number {
        return this._numConta;
    }
    public set numConta(value: number) {
        this._numConta = value;
    }

    public get saldo(): number {
        return this._saldo;
    }

    public get limite(): number {
        return this._limite;
    }

    public get tipo(): number {
        return this._tipo;
    }
    public set tipo(value: number) {
        this._tipo = value;
    }

    public get senha(): number {
        return this._senha;
    }
    public set senha(value: number) {
        this._senha = value;
    }
    
    depositar(deposito: number) : number{
        if (deposito > 0) {
            this._saldo = this._saldo + deposito;
            return this._saldo;
        }
    }

    sacar(saque: number) : number{
        if (saque <= this._saldo) {
            this._saldo = this._saldo - saque
            return this._saldo
        } else if (this._limite > 0 && saque <= this._limite) {
            this._saldo = this._saldo - saque;
            this._limite = this.limite - Math.abs(this._saldo);
            return this._saldo;
        } else {
            throw new Error("Sem limite para saque!");
        }
    }
}