import * as express from "express";
import { Banco } from "./banco";
import { Cliente } from "./cliente";
import { Conta } from "./conta";

const app = express();

app.use(express.urlencoded({extended: false}));
app.use(express.json());

const porta = 3001;
app.listen(porta, function () {
    console.log("Servidor rodando na porta " + porta);
});

app.get("/", function (req, resp) {
    resp.json({msn: "Servidor rodando"});
})

var conta: Conta;
var cliente: Cliente;
var banco: Banco;

app.post("/banco", function (req, resp) {
    var bancoUI = {
        nome: req.body.nmNomeBanco,
        cnpj: req.body.nmCnpj,
        agencia: req.body.nmAgencia
    }
    
    banco = new Banco(bancoUI.nome, bancoUI.cnpj, bancoUI.agencia, []);
    resp.send(banco);
});

app.get("/banco", function (req, resp) {
    resp.json({msn: banco});
});

app.post("/conta", function (req, resp) {
    var contaUI = {
        numConta: req.body.nmConta,
        saldo: req.body.nmSaldo,
        limite: req.body.nmLimite,
        tipo: req.body.nmTipo,
        senha: req.body.nmSenha
    }

    conta = new Conta(contaUI.numConta, contaUI.saldo, contaUI.limite, contaUI.tipo, contaUI.senha);
    resp.send(conta);
});

app.get("/conta", function (req, resp) {
    resp.json({msn: conta});
});

app.post("/cliente", function (req, resp) {
    var clienteUI = {
        nome: req.body.nmNomeCliente,
        cpf: req.body.nmCpf,
        rg: req.body.nmRg,
        endereco: req.body.nmEndereco
    }

    cliente = new Cliente(clienteUI.nome, clienteUI.cpf, clienteUI.rg, clienteUI.endereco, conta);
    banco.adicionaCliente(cliente);

    resp.send(cliente);
});

app.get("/cliente", function (req, resp) {
    resp.json({msn: cliente});
});

app.post("/depositar", function (req, resp) {
    let clienteSelecionado = banco.getCliente(req.body.nmContaCliente);
    
    let deposito = req.body.nmDeposito;
    clienteSelecionado.conta.depositar(deposito);
    resp.json({deposito});
});

app.post("/sacar", function (req, resp) {
    let clienteSelecionado = banco.getCliente(req.body.nmContaCliente);

    let saque = req.body.nmSaque;
    clienteSelecionado.conta.sacar(saque);
    resp.json({saque});
});

app.post("/extrato", function (req, resp) {
    let clienteSelecionado = banco.getCliente(req.body.nmContaCliente);

    let saldo = (clienteSelecionado.conta.saldo).toFixed(2);
    let limite = (clienteSelecionado.conta.limite).toFixed(2);

    resp.json({msn: "Saldo: R$" + saldo + "  |  Limite: R$" + limite});
});

app.get("/extrato", function (req, resp) {

    let clienteSelecionado = banco.getCliente(req.query.nmContaCliente as string);

    let saldo = (clienteSelecionado.conta.saldo).toFixed(2);
    let limite = (clienteSelecionado.conta.limite).toFixed(2);

    resp.json({msn: "Saldo: R$" + saldo + "  |  Limite: R$" + limite});
});